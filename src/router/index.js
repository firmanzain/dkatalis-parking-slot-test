import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [{
    path: '/',
    name: 'Landing',
    component: () =>
        import ( /* webpackChunkName: "landing" */ '../views/Landing'),
}, ]

const router = new VueRouter({
    mode: 'history',
    routes,
});

export default router;