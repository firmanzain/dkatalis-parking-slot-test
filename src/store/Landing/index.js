const state = {
    data: {
        parkingSlot: 0,
        parking: [],
    },
};

const mutations = {
    changeData(state, payload) {
        state.data = Object.assign({}, state.data, payload);
    },
};

const actions = {
    addParkingCar({ commit, state }, payload) {
        let parking = state.data.parking;
        let index = 0

        for (let i = 0; i < parking.length; i++) {
            if (parking[i].number == "") {
                index = i
                parking[i].number = payload.number
                parking[i].color = payload.color
                break;
            }
        }

        commit('changeData', {
            parking: parking,
        });
        return {
            slot: index + 1
        }
    },

    removeParkingCar({ commit, state }, payload) {
        let parking = state.data.parking;
        let index = -1

        for (let i = 0; i < parking.length; i++) {
            if (parking[i].number == payload.number) {
                index = i
                parking[i].number = ""
                parking[i].color = ""
                break;
            }
        }

        if (index == -1) {
            return false
        }

        commit('changeData', {
            parking: parking,
        });
        return {
            slot: index + 1
        }
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};