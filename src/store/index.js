import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import Landing from "./Landing";

export default new Vuex.Store({
    modules: {
        landing: Landing,
    },
    state: {},
    mutations: {},
    actions: {},
});